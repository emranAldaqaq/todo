import 'package:flutter/material.dart';

enum Type { Home, Collage, Jop, Trip }

class Todo with ChangeNotifier {
  String id;
  String type;
  String title;
  String description;
  DateTime dateTime;
  bool isChecked = false;
  Todo(
      {@required this.id,
      @required this.type,
      @required this.title,
      @required this.description,
      @required this.dateTime,
      this.isChecked});
  List<Todo> items = [
    // {
    //   "home": Todo(
    //     id: "4321",
    //     title: "Test",
    //     description: "This is a test",
    //     dateTime: DateTime.now(),
    //   )
    // },
  ];
  void addTodo(Todo todo) {
    items.add(Todo(
      id: DateTime.now().toString(),
      type: todo.type,
      title: todo.title,
      description: todo.description,
      dateTime: todo.dateTime,
    ));
    notifyListeners();
  }

  Todo findById(String id) {
    return items.firstWhere((element) => element.id == id);
  }
  //List<Todo> homeItems =[];

  // List<Todo> get homeTodos {
  //   for (int i = 0; i <= items.length; i++)
  //     if (items[i].type == "Type.home")
  //       homeItems.add(items[i]);
  //   notifyListeners();
  //   return homeTodos;
  // }
  bool todoChecked(Todo todo) {
    return todo.isChecked; 
  }
  void toggleChecked(String id,bool b) {
    final todoIndex = items.indexWhere((todo) => todo.id == id);
    if (todoIndex != null) {
      // _items[todoIndex] = newtodo;
      items[todoIndex].isChecked = b;
    }
  }
    void updateTodo(String id, Todo newtodo) {
      final todoIndex = items.indexWhere((todo) => todo.id == id);
      if (todoIndex != null) {
        print(newtodo.isChecked);
        items[todoIndex] = newtodo;
        print(items[todoIndex].isChecked);
        notifyListeners();
      } else {
        print('Error in update method in file todos');
      }
    }

    // static Todo fromMap(Map<String, dynamic> temp) {
    //   Todo todo = new Todo(
    //     id: temp['id'],
    //     type: temp['type'],
    //     description: temp['description'],
    //     title: temp['title'],
    //     isChecked: temp['is_checked'],
    //     dateTime: DateTime.now(),
    //   );
    //   return todo;
    // }
  }

