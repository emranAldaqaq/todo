import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/providers/todo.dart';
import 'package:todoapp/screens/updateTodoScreen.dart';

class TodoItem extends StatelessWidget {
  // final List<Map<String, Todo>> todosofType;
  // TypeItem(this.todosofType);
  
  Widget build(BuildContext context) {
    final _todoData = Provider.of<Todo>(context, listen: false);
  
    return _todoData.items.isEmpty
        ? Center(
            child: Text(
            "Add New Todo's",
            style: TextStyle(
              color: Colors.black.withOpacity(0.3),
            ),
          ))
        : ListView.builder(
            itemCount: _todoData.items.length,
            itemBuilder: (ctx, i) {
              return Card(
                elevation: 3.0,
                margin: EdgeInsets.all(10.0),
                child: ListTile(
                  title: Text(_todoData.items[i].title),
                  subtitle:
                      Text(_todoData.items[i].type.toString().substring(5)),
                  trailing: Text(
                      _todoData.items[i].dateTime.toString().substring(0, 16)),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (_) => AlertDialog(
                              title: Text("Update Todo"),
                              content: UpdateTodoScreen(_todoData.items[i]),
                              actions: [
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("Close"),
                                ),
                              ],
                            ));
                  },
                ),
              );
            });
  }
}


