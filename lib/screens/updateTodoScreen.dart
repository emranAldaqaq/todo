import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/providers/todo.dart';

class UpdateTodoScreen extends StatefulWidget {
  Todo existingTodo;
  UpdateTodoScreen(this.existingTodo);
  @override
  _UpdateTodoScreenState createState() => _UpdateTodoScreenState();
}

final GlobalKey<FormState> _fromkey = GlobalKey<FormState>();
final _descriptionFocusNode = FocusNode();

class _UpdateTodoScreenState extends State<UpdateTodoScreen> {
  Map<String, dynamic> _initValues = {
    'title': '',
    'description': '',
    'is_checked': false,
  };
  var _editedTodo =
      Todo(id: '',type:'',title: '', description: ' ', dateTime: DateTime.now(),isChecked:false);
  var _isInit = true;
  bool checked = false;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      final todoId = widget.existingTodo.id;
      if (todoId != null) {
        _editedTodo =
            Provider.of<Todo>(context, listen: false).findById(todoId);
        var _initValues = {
          'id': _editedTodo.id.toString(),
          'type':_editedTodo.type,
          'title': _editedTodo.title,
          'description': _editedTodo.description,
          'is_checked': _editedTodo.isChecked,
        };
        print(_initValues);
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    final _todoData = Provider.of<Todo>(context, listen: false);
    void onSaved() {
      if (!_fromkey.currentState.validate()) return;
      _fromkey.currentState.save();
      _todoData.updateTodo(_editedTodo.id,_editedTodo);
      Navigator.of(context).pop();
    }

    return Form(
      key: _fromkey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          //title
          TextFormField(
            initialValue:_editedTodo.title,
            decoration: InputDecoration(labelText: "Enter Title"),
            validator: (value) {
              if (value.isEmpty) return "Name is Request.";
            },
            onSaved: (value) {
              _editedTodo = Todo(
                id:_editedTodo.id,
                type:_editedTodo.type,
                title: value,
                description:_editedTodo.description,
                dateTime:_editedTodo.dateTime,
                isChecked:_editedTodo.isChecked,
              );
            },
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) {
              FocusScope.of(context).requestFocus(_descriptionFocusNode);
            },
          ),
          //description
          TextFormField(
            initialValue:_editedTodo.description,
            decoration: InputDecoration(labelText: "Enter Description"),
            maxLines: 3,
            keyboardType: TextInputType.multiline,
            validator: (value) {
              if (value.isEmpty) return "Description is Request.";
              if (value.length < 10)
                return "Should be at least 10 characters long.";
              return null;
            },
            focusNode: _descriptionFocusNode,
            onSaved: (value) {
              _editedTodo= Todo(
                id:_editedTodo.id,
                type:_editedTodo.type,
                title:_editedTodo.title,
                description: value,
                dateTime:_editedTodo.dateTime,
                isChecked:_editedTodo.isChecked,
              );
            },
          ),
          SizedBox(
            height: 10,
          ),
          //Checked
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Done"),
              Checkbox(
                    value:widget.existingTodo.todoChecked(_editedTodo),
                    onChanged: (ch) {
                      setState(() {
                        //checked = ch;
                        print("ch is :$ch");
                        //print("checked is :$checked");
                        
                         _initValues['is_checked'] = ch;
                         _editedTodo.isChecked = ch;
                         //_editedTodo.toggleChecked(_editedTodo.id,ch);
                      });
                    },
                  ),
              SizedBox(
                height: 15,
              ),
              FlatButton(
                onPressed: () {
                  onSaved();
                },
                child: Text("Save Todo"),
              )
            ],
          ),
        ],
      ),
    );
  }
}
