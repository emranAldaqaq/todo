
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/providers/todo.dart';

class AddTodoScreen extends StatelessWidget {
  final Type todoType;
  AddTodoScreen(this.todoType);

  final GlobalKey<FormState> _fromkey = GlobalKey<FormState>();

  final _descriptionFocusNode = FocusNode();

  var _editingTodo = Todo(
      id: DateTime.now().toString(),
      type:null,
      title: "",
      description: "",
      dateTime: DateTime.now(),
      isChecked: false,
    );

  @override
  Widget build(BuildContext context) {
    final _todoData = Provider.of<Todo>(context,listen: false);

    void onSaved() {
    if (!_fromkey.currentState.validate()) return;
    _fromkey.currentState.save();
    _editingTodo.isChecked = false;
    _todoData.addTodo(_editingTodo);
    Navigator.of(context).pop();
    print("\n\n********");
    print("TODO ID:${_editingTodo.id}");
    print("TODO type:${_editingTodo.type}\n");
    print("TODO title:${_editingTodo.title}");
    print("TODO description:${_editingTodo.description}");
    print("TODO checked:${_editingTodo.isChecked}");
    print("******");
    
  }
  
    return Scaffold(
      backgroundColor:Color.fromRGBO(151, 202, 219, 1),
      appBar: AppBar(
        backgroundColor:Color.fromRGBO(2, 25, 75, 1),
        title: Text("Add a New ${todoType.toString().substring(5)} Todo"),
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: Form(
          key: _fromkey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              //title
              TextFormField(
                decoration: InputDecoration(labelText: "Enter Title"),
                validator: (value) {
                  if (value.isEmpty) return "Name is Request.";
                },
                onSaved: (value) {
                  _editingTodo = Todo(
                    id: _editingTodo.id,
                    type: todoType.toString(),
                    title: value,
                    description: _editingTodo.description,
                    dateTime: _editingTodo.dateTime,
                  );
                },
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_descriptionFocusNode);
                },
              ),
              //description
              TextFormField(
                decoration: InputDecoration(labelText: "Enter Description"),
                maxLines: 3,
                keyboardType: TextInputType.multiline,
                validator: (value) {
                  if (value.isEmpty) return "Description is Request.";
                  if (value.length < 10)
                    return "Should be at least 10 characters long.";
                  return null;
                },
                focusNode: _descriptionFocusNode,
                onSaved: (value) {
                  _editingTodo = Todo(
                    id:_editingTodo.id,
                    type: todoType.toString(),
                    title:_editingTodo.title,
                    description:value,
                    dateTime:_editingTodo.dateTime,
                  );
                },
              ),
              SizedBox(
                height: 10,
              ),
              //Checked
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Done"),
                  Checkbox(
                    value: _editingTodo.isChecked,
                    onChanged: (newvalue) {
                       // _editingTodo.isChecked = newvalue;
                    },
                    //_initValues['is_checked'],
                    // onChanged: (ch) {
                    //   setState(() {
                    //     //_initValues['isChecked'] = ch;
                    //   });
                    // },
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  FlatButton(
                    onPressed: () {
                      onSaved();
                      
                    },
                    child: Text("Save"),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
