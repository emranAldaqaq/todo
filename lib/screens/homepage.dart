import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/items/todo_item.dart';
import 'package:todoapp/providers/todo.dart';
import 'package:todoapp/screens/addTodoscreen.dart';
import 'package:todoapp/screens/loginandsignupscreen.dart';

bool notopened = true;

class HomePage extends StatefulWidget {
  static const routeName = '/home';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void didChangeDependencies() {
    notopened = true;
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    final _todoData = Provider.of<Todo>(context, listen: false);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(151, 202, 219, 1),
        appBar: AppBar(
          backgroundColor:Color.fromRGBO(2, 25, 75, 1),
          title: Text("-- Todo's List --"),
        ),
        drawer: Drawer(
          child: Column(
            children: [
              ListTile(
                title: Text("Home Page"),
                leading: Icon(Icons.home),
              ),
              ListTile(
                title: Text("Login & Signup"),
                leading: Icon(Icons.logout),
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(LoginAndSignupScreen.routeName);
                },
              ),
            ],
          ),
        ),
        body: Center(
          child: Stack(
            overflow: Overflow.visible,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Card(
                  elevation: 2,
                  child: Container(
                    color: Color.fromRGBO(2, 137, 192, 1),
                    width: (MediaQuery.of(context).size.width) - 20,
                    height: 580,
                    child: TodoItem(),
                  ),
                ),
              ),

              //Add Home Todos
              AnimatedPositioned(
                duration: Duration(seconds: 1),
                curve: Curves.fastLinearToSlowEaseIn,
                left: notopened
                    ? MediaQuery.of(context).size.width / 2 - 40
                    : MediaQuery.of(context).size.width / 2 - 110.0,
                top: notopened
                    ? (MediaQuery.of(context).size.height / 2) + 140.0
                    : (MediaQuery.of(context).size.height / 2) + 50.0,
                child: _buildOption("Home", context, Type.Home),
              ),
              //Add Job Todos
              AnimatedPositioned(
                duration: Duration(seconds: 1),
                curve: Curves.fastLinearToSlowEaseIn,
                left: notopened
                    ? (MediaQuery.of(context).size.width / 2) - 40.0
                    : MediaQuery.of(context).size.width / 2 + 20.0,
                top: notopened
                    ? (MediaQuery.of(context).size.height / 2) + 150.0
                    : (MediaQuery.of(context).size.height / 2) + 50.0,
                child: _buildOption("Job", context, Type.Jop),
              ),
              //Add Collage Todos
              AnimatedPositioned(
                duration: Duration(seconds: 1),
                curve: Curves.fastLinearToSlowEaseIn,
                left: notopened
                    ? MediaQuery.of(context).size.width / 2 - 40.0
                    : (MediaQuery.of(context).size.height / 2) - 120.0,
                top: (MediaQuery.of(context).size.height / 2) + 150.0,
                child: _buildOption("Collage", context, Type.Collage),
              ),
              //Add Trip Todos
              AnimatedPositioned(
                duration: Duration(seconds: 1),
                curve: Curves.fastLinearToSlowEaseIn,
                left: notopened
                    ? MediaQuery.of(context).size.width / 2 - 40.0
                    : (MediaQuery.of(context).size.height / 2) - 340.0,
                top: (MediaQuery.of(context).size.height / 2) + 150.0,
                child: _buildOption("Trip", context, Type.Trip),
              ),

              AnimatedPositioned(
                duration: Duration(seconds: 1),
                left: MediaQuery.of(context).size.width / 2 - 70.0,
                top: (MediaQuery.of(context).size.height / 2) + 130.0,
                child: Align(
                  alignment: Alignment.center,
                  child: AnimatedSwitcher(
                    duration: Duration(milliseconds: 200),
                    transitionBuilder:
                        (Widget child, Animation<double> animation) {
                      return ScaleTransition(
                        scale: animation,
                        child: child,
                      );
                    },
                    child: notopened
                        ? InkWell(
                            key: UniqueKey(),
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Container(
                                width: 105.0,
                                height: 105.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100.0),
                                  color: Color.fromRGBO(2, 25, 75, 1),
                                ),
                                child: Center(
                                    child: Text(
                                  "Add Todo",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                )),
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                notopened = !notopened;
                              });
                              //print("HomeTodo length is : ${_todoData.homeItems.length}");
                              //print("HomeTodo is : ${_todoData.homeTodos.first}");
                              // print("Todos length is : ${_todoData.items.length}");
                              // print("Todos is : ${_todoData.items}");
                            },
                          )
                        : InkWell(
                            key: UniqueKey(),
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Container(
                                width: 110.0,
                                height: 110.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100.0),
                                  color:Color.fromRGBO(2, 25, 75, 1),
                                ),
                                child: Center(
                                  child: Icon(
                                    Icons.blur_on,
                                    color: Colors.white,
                                    size: 40,
                                  ),
                                ),
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                notopened = !notopened;
                              });
                              //print("HomeTodo length is : ${_todoData.homeTodos..length}");
                              //print("HomeTodo is : ${_todoData.homeTodos.first}");
                              // print("Todos length is : ${_todoData.items.length}");
                              // print("Todos is : ${_todoData.items}");
                            },
                          ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buildOption(String title, BuildContext context, Type type) {
  return AnimatedSwitcher(
    duration: Duration(milliseconds: 100),
    transitionBuilder: (Widget child, Animation<double> animation) {
      return RotationTransition(
        turns: animation,
        child: child,
      );
    },
    child: InkWell(
      key: UniqueKey(),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          height: 70.0,
          width: 70.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40.0),
            color: Color.fromRGBO(5, 70, 124, 1),
          ),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 15),
            ),
          ),
        ),
      ),
      onTap: () {
        //Navigator.of(context).pushNamed(AddTodoScreen.routeName,arguments: type.toString());
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddTodoScreen(
                type,
              ),
            ));
      },
    ),
  );
}
