import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/screens/homepage.dart';
import 'package:todoapp/screens/loginandsignupscreen.dart';
import './providers/todo.dart';
void main() {
  runApp(Provider<Type>.value(value:Type.Home,child:MyApp(),));
  
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
        //ChangeNotifierProvider.value(value: Auth()),
        ChangeNotifierProvider.value(value: Todo()),
      ],
          child: MaterialApp(
        debugShowCheckedModeBanner: false,
        
        //home:HomePage(),
        routes: {
          //AddTodoScreen.routeName: (ctx) => AddTodoScreen(),
          "/" :(ctx) =>HomePage(),
          LoginAndSignupScreen.routeName :(ctx) => LoginAndSignupScreen()
          },
      ),
    );
  }
}